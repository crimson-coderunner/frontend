import Vue from 'vue'
import Vuex from 'vuex'
import Config from './config.js'
import API from './api.js'
import Util from './util.js'

Vue.use(Vuex)
// TODO: runSandbox seems to have failed when called. fix this.
const _resolveIconType = (group) => (
  group === 'Project'? 'box'
  : group === 'Build'? 'target'
  : group === 'Run'? 'run'
  : null
)
const _mkaction = (id, name, icontype, callback) => ({id:id,name:name,iconType:icontype,callback:callback})
const _mkSandboxAction = (group, id, cmd, conf) => {
  let name = conf.name;
  return {
    id:id, name: name, iconType: _resolveIconType(group),
    callback: () => {
      let additional = {};
      if (conf.ask) {
        for (let i in conf.ask) {
          additional[conf.ask[i].name] = prompt(conf.ask[i].prompt);
        }
      }
      STORE.dispatch('runAction', {
        lang: STORE.state.currentLanguage,
        group: group,
        action: cmd,
        filename: Util.getPath(STORE.state.currentSelection),
        dirname: STORE.state.pwd,
        additional: additional,
      })
    }
  }
}
const _mkSandboxActionBar = (group, actionList) => {
  let res = [];
  let nextId = 0;
  for (let i in actionList) {
    res.push(_mkSandboxAction(group, nextId++, i, actionList[i]))
  }
  return res;
}

const STORE = new Vuex.Store({
  state: {
    currentUsername: null,
    currentSessionId: null,
    currentSelection: null,

    pwd: '.',
    currentFile: null,

    editor: null,
    terminal: null,
    terminalHeartbeat: null,
    socket: null,
    currentPage: null,
    workspacePage: 'editor',
    loggedIn: false,
    dirTree: null,
    
    currentLanguage: null,
    sandboxStatusMsg: null,
    currentErrorMsg: null,


    currentAction: FILE_ACTION,
    currentProjectAction: null,
    currentBuildAction: null,
    currentRunAction: null
  },
  getters: {
    parentPath (state) {
      return state.currentSelection? state.currentSelection.inhabitPath : '';
    }
  },
  mutations: {
    setCurrentPage (state, pagename) {
      state.currentPage = pagename;
    },
    setCurrentUsername(state, username) {
      state.currentUsername = username;
    },
    setCurrentSessionId (state, sessionId) {
      state.currentSessionId = sessionId;
    },
    setEditor (state, editor) {
      state.editor = editor;
    },

    setSandboxStatusMsg (state, msg) {
      state.sandboxStatusMsg = msg;
    },
    setHeartbeat (state, heartbeat) {
      state.terminalHeartbeat = heartbeat;
    },
    setErrorMessage (state, errormsg) {
      state.currentErrorMsg = errormsg;
      
    },
    setCurrentFile (state, filepath) {
      state.currentFile = filepath;
    },


    _setTerminal (state, terminal) {
      state.terminal = terminal;
    },
    _setWorkspacePage (state, pagename) {
      state.workspacePage = pagename;
    },
    _setDirTree (state, dirtree) {
      state.dirTree = dirtree;
    },
    _setPwd (state, pwd) {
      state.pwd = pwd;
    },
    _setCurrentLanguage (state, langname) {
      state.currentLanguage = langname;
    },
    _setSocket (state, socket) {
      state.socket = socket;
    },
  

    deselectCurrent (state) {
      if (state.currentSelection) {
        state.currentSelection.selected = false;
      }
      state.currentSelection = null;
      state.pwd = '.';
    },
    select(state, content) {
      if (state.currentSelection) {
        state.currentSelection.selected = false;
      }
      content.selected = true;
      state.currentSelection = content;
      state.pwd = content.inhabitPath;
    },

    _setAction(state, action) {
      state.currentAction = action;
    },
    setCurrentProjectAction(state, action) {
      state.currentProjectAction = action;
    },
    setCurrentBuildAction(state, action) {
      state.currentBuildAction = action;
    },
    setCurrentRunAction(state, action) {
      state.currentRunAction = action;
    }
  },
  actions: {
    login (context, payload) {
      API.login(payload.username, payload.password)
      .then((res) => {
        context.commit('setCurrentUsername', payload.username);
        context.commit('setCurrentSessionId', res.content.session_id);
        Util.setCookie('username', payload.username);
        Util.setCookie('session_id', res.content.session_id);
        context.dispatch('getDirTree');
        context.commit('setCurrentPage', Config.constants.page.workspacePage);
      }).catch((reason) => {
        alert(`Failed to login: ${reason}`)
      })
    },
    logOut (context) {
      API.logout()
      .then(() => {
        context.commit('setCurrentUsername', null);
        context.commit('setCurrentSessionId', null);
        Util.deleteCookie('username');
        Util.deleteCookie('session_id');
        context.commit('setCurrentPage', Config.constants.page.descriptionPage);
      })
    },
    signup (context, payload) {
      API.signup(payload.username, payload.password)
      .then(() => {
        context.dispatch('login', payload);
      }).catch((reason) => {
        alert(`Failed to register with reason ${JSON.stringify(reason)}. Please contact the webmaster.`)
      })
    },
    switchToLogin (context) {
      context.commit('setCurrentPage', Config.constants.page.loginPage);
    },
    switchToDescription (context) {
      context.commit('setCurrentPage', Config.constants.page.descriptionPage);
    },
    switchToSignup (context) {
      context.commit('setCurrentPage', Config.constants.page.signupPage);
    },
    setTerminal (context, terminal) {
      context.commit('_setTerminal', terminal);
    },
    showEditor (context) {
      context.commit('_setWorkspacePage', 'editor');
    },
    showConsole (context) {
      context.commit('_setWorkspacePage', 'console');
    },
    refreshCredential (context) {
      context.commit('setCurrentUsername', Util.getCookie('username'))
      context.commit('setCurrentSessionId', Util.getCookie('session_id'))
    },
    getDirTree (context) {
      API.getDirTree(
        Util.getCookie('username'),
        Util.getCookie('session_id')
      ).then((res) => {
          context.commit('_setDirTree', Util.dirTree2Structure(res.content.result));
      }).catch((reason) => {
        alert(`Failed to load dirtree with reason ${JSON.stringify(reason)}. Please contact the webmaster.`)
      })
    },
    newFile (context, filename) {
      API.newFile(
        context.state.pwd,
        filename
      ).then(() => {
        alert('File created.');
        context.dispatch('getDirTree')
      }).catch((reason) => {
        alert(`Failed to create file with reason ${reason.status}:${reason.statusText}`);
      });
    },
    newFolder (context, foldername) {
      API.newFolder(
        context.state.pwd,
        foldername
      ).then(() => {
        alert('Folder created.');
        context.dispatch('getDirTree')
      }).catch((reason) => {
        alert(`Failed to create file with reason ${reason.status}:${reason.statusText}`);
      });
    },
    delete (context, obj) {
      API.delete(
        Util.getPath(obj)
      ).then(() => {
        alert('Deleted.');
        context.dispatch('getDirTree')
      }).catch((reason) => {
        alert(`Failed to delete file with reason ${reason.status}:${reason.statusText}`);
      });
    },
    saveFile (context) {
      API.saveFile(
        context.state.currentSelection,
        context.state.editor
      ).then(() => {
        alert('Saved.');
      }).catch((reason) => {
        alert(`Failed to save file with reason ${reason.status}:${reason.statusText}`);
      });
    },
    useLanguage (context, extname) {
      if (!window.editor) return;
      let langname = Util.resolveLangname(extname);
      context.commit('_setCurrentLanguage', langname);
      window.editor.getSession().setMode(Util.resolveAceMode(extname));
      // TODO: set menu bar.
      API.getLanguageMenu(langname)
      .then((value) => {
        value = value.content;
        context.commit('setCurrentProjectAction', _mkSandboxActionBar('Project', value.Project));
        context.commit('setCurrentBuildAction', _mkSandboxActionBar('Build', value.Build));
        context.commit('setCurrentRunAction', _mkSandboxActionBar('Run', value.Run));
      })
    },
    rename (context, payload) {
      let { oldName, newName } = payload;
      API.rename(
        context.state.currentSelection.inhabitPath,
        oldName,
        newName
      ).then(() => {
        context.dispatch('getDirTree')
        alert('Renamed.');
      }).catch((reason) => {
        alert(`Failed to rename file with reason ${reason.status}:${reason.statusText}`);
      })
    },
    connect (context, addr) {
      let socket = new WebSocket(addr);
      socket.onmessage = function (event) {
        let data = event.data;
        if (data[0] === '3') {
          context.commit('setSandboxStatusMsg', data.slice(1));
        } else if (data[0] === '2') {
          context.state.terminal.write(data.slice(1))
        } else if (data[0] === '4') {
          alert('Container stopped running.')
          context.dispatch('stopHeartbeat')
          context.dispatch('getDirTree')
        }
      }
      context.commit('_setSocket', socket);      
    },
    runAction (context, payload) {
      let lang = payload.lang;
      let group = payload.group;
      let action = payload.action;
      let filename = payload.filename;
      let dirname = payload.dirname;
      context.state.terminal.clear();
      API.runSandbox(lang, group, action, filename, dirname, payload.additional)
      .then((msg) => {
        context.commit('setSandboxStatusMsg', 'Sandbox request sent. Waiting for response...');
        context.commit('setHeartbeat', window.setInterval(() => {
          context.state.socket.send('0')
        }, 2000));
        context.state.socket.send(`1${msg.content.container_name}`);
      }).catch((reason) => {
        alert(`Failed to run command with reason ${reason.status}:${reason.statusText}`)
      })
    },
    stopHeartbeat (context) {
      window.clearInterval(context.state.terminalHeartbeat);
    },
    setActionToFileAction (context) {
      context.commit('_setAction', FILE_ACTION);
    },
    setActionToEditorAction (context) {
      context.commit('_setAction', EDITOR_ACTION);
    },
    setActionToProjectAction (context) {
      context.commit('_setAction', context.state.currentProjectAction);
    },
    setActionToBuildAction (context) {
      context.commit('_setAction', context.state.currentBuildAction);
    },
    setActionToRunAction (context) {
      context.commit('_setAction', context.state.currentRunAction);
    }
  }
});
export default STORE;

const newFile = (function () {
  if (!this.state.currentSelection) {
    alert('No selected parent folder. Will create in root folder.');
  }
  this.dispatch('newFile', prompt('File name:'));
}).bind(STORE);
const newFolder = (function () {
  if (!this.state.currentSelection) {
    alert('No selected parent folder. Will create in root folder.');
  }
  this.dispatch('newFolder', prompt('Folder name:'));
}).bind(STORE);
const rename = (function () {
  let oldName = this.state.currentSelection.path;
  let newName = prompt(`Current name: ${oldName}, new name:`);
  this.dispatch('rename', { oldName: oldName, newName: newName });
}).bind(STORE);
const saveFile = (function () { this.dispatch('saveFile') }).bind(STORE);
const uploadFile = (function () { }).bind(STORE);
const downloadFile = (function () { }).bind(STORE);
const deletePath = (function () { this.dispatch('delete', this.state.currentSelection); }).bind(STORE);
let actionId = 0;
const FILE_ACTION = [
  _mkaction(actionId++, "New File", "newfile", newFile),
  _mkaction(actionId++, "New Folder", "newfolder", newFolder),
  _mkaction(actionId++, "Rename", "rename", rename),
  _mkaction(actionId++, "Save", "save", saveFile),
  _mkaction(actionId++, "Upload file", "upload-cloud", uploadFile),
  _mkaction(actionId++, "Download file", "download-cloud", downloadFile),
  _mkaction(actionId++, "Delete", "delete", deletePath),
];
const cut = (function () {
  /*
  copy();
  let acee = STORE.state.editor;
  acee.session.replace(acee.session.getSelection().getRange(), '')
  */
  if (document.execCommand('cut')) {
    alert('Your browser does not support execCommand cut. Please use Ctrl/Command + X.');
  }
}).bind(STORE);
const copy = (function () {
  if (document.execCommand('copy')) {
    alert('Your browser does not support execCommand copy. Please use Ctrl/Command + C.');
  }
}).bind(STORE);
const paste = (function () {
  if (document.execCommand('paste')) {
    alert('Your browser does not support execCommand paste. Please use Ctrl/Command + V.');
  }
}).bind(STORE);
const undo = (function () {
  STORE.state.editor.undo();
}).bind(STORE);
const redo = (function () {
  STORE.state.editor.redo();
}).bind(STORE);
const EDITOR_ACTION = [
  _mkaction(actionId++, "Cut", "scissors", cut),
  _mkaction(actionId++, "Copy", "copy", copy),
  _mkaction(actionId++, "Paste", "clipboard", paste),
  _mkaction(actionId++, "Undo", "corner-right-up", undo),
  _mkaction(actionId++, "Redo", "corner-right-down", redo)
];



