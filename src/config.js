export default {
    hostname: 'http://127.0.0.1:8000',
    websocket: 'ws://127.0.0.1:8000/ws',
    constants: {
        page: {
            descriptionPage: 'description',
            loginPage: 'login',
            workspacePage: 'workspace',
            signupPage: 'signup'
        }
    }
}