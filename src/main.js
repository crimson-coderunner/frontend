import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import store from './store'
import 'xterm/dist/xterm.css'
import API from './api';


Vue.use(Vuex);
Vue.config.productionTip = false

new Vue({
  store,
  render: h => h(App),
  mounted () {
    let prevCredential = API.getCredential();
    if (prevCredential.username && prevCredential.session_id) {
      API.checkCredential(prevCredential.username, prevCredential.session_id)
      .then((res) => {
        if (res.content.result) {
          this.$store.commit('setCurrentPage', 'workspace')
          this.$store.dispatch('refreshCredential')
          API.APIRequest('file', {
             action: 'list',
             name: this.$store.state.currentUsername,
             path: '.',
          }, API.getCredential())
          .then((res) => {
// TODO:
// 2. 将APIu全部修整一遍，这样错误就可以用ErrorPage做，不需要alert了。
          })
          .catch((reason) => {
            this.$store.commit('setErrorMessage',
              'Failed to load the workspace. Maybe the server is down at the moment.'
            );
            this.$store.commit('setCurrentPage', 'error')
          })
          this.$store.dispatch('getDirTree')
        } else {
          API.clearCredential();
          this.$store.commit('setCurrentPage', 'description');
        }
      })
      .catch(() => {
        API.clearCredential();
        this.$store.commit('setCurrentPage', 'description');
      })
    } else {
      this.$store.commit('setCurrentPage', 'description');
    }
    
  }
}).$mount('#app');
