import store from './store.js';
import Config from './config.js';
import Util from './util.js';

export default class API {
    static APIRequest(type, content, credential) {
        return Util.asyncReq(
            Config.hostname + '/api',
            'POST',
            null,
            {
                msg: {
                    type: type,
                    credential: credential,
                    content: content
                }
            }
        );
    }
    static getCredential() {
        return {
            username: Util.getCookie('username'),
            session_id: Util.getCookie('session_id')
        }
    }
    static checkCredential(username, session_id) {
        return API.APIRequest('integrity', {
            username: username,
            session_id: session_id
        })
    }
    static clearCredential() {
        Util.setCookie('username', null);
        Util.setCookie('session_id', null);
    }
    static login(username, password) {
        return API.APIRequest('login', {
            username: username,
            password: password
        });
    }
    static logout() {
        return API.APIRequest('logout',{
            username: store.state.currentUsername
        }, API.getCredential());
    }
    static signup(username, password) {
        return API.APIRequest('signup', {
            username: username, password: password
        });
    }
    static newFile(path, filename) {
        return API.APIRequest('file', {
            action: 'new',
            path: path,
            filename: filename
        }, API.getCredential());
    }
    static newFolder(path, foldername) {
        return API.APIRequest('file', {
            action: 'new-folder',
            path: path,
            foldername: foldername
        }, API.getCredential())
    }
    static getDirTree() {
        return API.APIRequest('file', {
            action: 'list',
            path: '.',
        }, API.getCredential());
    }
    static delete(path) {
        return API.APIRequest('file', {
            action: 'delete',
            path: path
        }, API.getCredential())
    }
    static getFile(filepath) {
        return API.APIRequest('file', {
            action: 'get',
            path: filepath,
        }, API.getCredential())
    }
    static saveFile(obj, editor) {
        return API.APIRequest('file', {
            action: 'update',
            path: Util.getPath(obj),
            content: editor.getValue()
        }, API.getCredential())
    }
    static rename(inhabitPath, oldName, newName) {
        return API.APIRequest('file', {
            action: 'rename',
            path: inhabitPath,
            oldname: oldName,
            newname: newName
        }, API.getCredential())
    }
    static runSandbox(lang, group, action, filename, dirname, additional) {
        return API.APIRequest('sandbox', {
            action: 'allocate',
            lang: lang,
            group: group,
            command: action,
            dirname: dirname,
            filename: filename,
            additional: additional,
        }, API.getCredential())
    }
    static getSupportedLanguage() {
        return API.APIRequest('lang', {
            action: 'list_lang'
        }, null);
    }
    static getLanguageMenu(lang) {
        return API.APIRequest('lang', {
            action: 'load_menu',
            lang: lang
        }, null);
    }
}