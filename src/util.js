

export default class Util {
    static arrayMap(array, f) {
        let res = [];
        for (let i in array) {
            res.push(f(array[i]));
        }
        return res;
    }
    static asyncReq(url, method, cookie, payload) {
        return new Promise((resolve, reject) => {
            let req = new XMLHttpRequest();
            req.addEventListener('error', () => {
                reject({
                    status: req.status,
                    statusText: req.statusText
                });
            });
            req.addEventListener('load', () => {
                let resp = JSON.parse(req.responseText);
                if (resp.type === 'error') {
                    reject(resp.content.msg);
                }
                resolve(resp);
            });
            let data = new FormData();
            for (let fieldkey in payload) {
                data.append(fieldkey, JSON.stringify(payload[fieldkey]));
            }
            req.open(method, url);
            if (cookie) {
                req.withCredentials = true;
            }
            req.send(data);
        });
    }
    static mkFormData(dict) {
        let res = new FormData();
        for (let dictkey in dict) {
            res.append(dictkey, dict[dictkey]);
        }
        return res;
    }

    static dict2cookie(dict) {
        let keys = [];
        for (let dictkey in dict) {
            keys.push(dictkey);
        }
        return keys.map((f) => `${f}=${dict[f]}`).join('; ');
    }
    static cookie2dict(cookie) {
        let splitted = cookie.split(';');
        let mapped = splitted.map((x) => (
            x.replace(/([^=]*)=(.*)$/, `"$1":"$2"`)
        ));
        return JSON.parse(`{${mapped.join(',')}}`)
    }

    static dirTree2Structure(dirtree) {
        console.log(dirtree)
        let currentId = 0;
        function _mkentry(path, parent, parentPath, content, is_dir) {
            let res = Object.create(null);
            res.id = currentId++;
            res.path = path;
            res.inhabitPath = parentPath;
            
            res.selected = false;
            res.parent = parent;
            res.content = content;
            res.isdir = is_dir? true : false; // is_dir在这里是null | object，不是boolean
            
            return res;
        }
        function _rec(parent, parentPath, obj) {
            let res = [];
            for (let path in obj) {
                let head = _mkentry(path, parent, `${parentPath}`, obj[path] ? _rec(head, `${parentPath}/${path}`, obj[path]) : null, obj[path]);
                res.push(head);
            }
            return res;
        }
        let head = _mkentry('.', null, null, null, true);
        let content = _rec(head, '.', dirtree);
        head.content = content;
        console.log(head)
        return head;
    }

    static setCookie(key, value) {
        document.cookie = `${key}=${value}`
    }
    static getCookie(c_name) {
        return document.cookie.replace(
            new RegExp(`(?:(?:^|.*;\\s*)${c_name}\\s*\\=\\s*([^;]*).*$)|^.*$`),
            "$1"
        );
        
    }
    static deleteCookie(key) {
        document.cookie = `${key}=null;expires=Thu, 01 Jan 1970 00:00:00 GMT`
    }

    static getPath(obj) {
        return `${obj.inhabitPath}/${obj.path}`
    }

    static resolveLangname(extname) {
        return LANG_NAME_DICT[extname]? LANG_NAME_DICT[extname]: 'Plain_Text';
    }

    static resolveAceMode(extname) {
        return 'ace/mode/' + (ACE_MODE_RESOLVE_DICT[extname]? ACE_MODE_RESOLVE_DICT[extname] : 'plain_text');
    }

    
}

const LANG_NAME_DICT = {
    py: 'Python',
    r: 'R',
    red: 'Red',
    rst: 'RestructuredText',
    rs: 'Rust',
    sass: 'Sass',
    scss: 'Scss',
    scm: 'Scheme',
    sh: 'Shell',
    sql: 'SQL',
    svg: 'SVG',
    tex: 'LaTeX',
    toml: 'TOML',
    ts: 'TypeScript',
    xml: 'XML',
    yaml: 'yaml',
    php: 'PHP',
    pas: 'Pascal',
    ml: 'OCaml',
    md: 'Markdown',
    lua: 'Lua',
    kt: 'Kotlin',
    jl: 'Julia',
    json: 'JSON',
    jsp: 'JSP',
    js: 'Javascript',
    java: 'Java',
    class: 'Java',
    gradle: 'Java',
    io: 'Io',
    html: 'HTML',
    hs: 'Haskell',
    groovy: 'Groovy',
    go: 'Go',
    fs: 'F#',
    for: 'Fortran',
    erl: 'Erlang',
    d: 'D',
    css: 'CSS',
    cs: 'C#',
    c: 'C_C++',
    h: 'C_C++',
    cpp: 'C_C++',
    hpp: 'C_C++',
    bat: 'Batchfile',
    asm: 'X86_Assembly',
};

const ACE_MODE_RESOLVE_DICT = {
    py: 'python',
    r: 'r',
    red: 'red',
    rst: 'rst',
    rs: 'rust',
    sass: 'sass',
    scss: 'scss',
    scm: 'scheme',
    sh: 'sh',
    sql: 'sql',
    svg: 'svg',
    tex: 'latex',
    toml: 'tom',
    ts: 'typescript',
    xml: 'xml',
    yaml: 'yaml',
    php: 'php',
    pas: 'pascal',
    ml: 'ocaml',
    md: 'markdown',
    lua: 'lua',
    kt: 'kotlin',
    jl: 'julia',
    json: 'json',
    jsp: 'jsp',
    js: 'javascript',
    java: 'java',
    io: 'io',
    html: 'html',
    hs: 'haskell',
    groovy: 'groovy',
    gradle: 'groovy',
    go: 'golang',
    fs: 'fsharp',
    for: 'fortran',
    erl: 'erlang',
    d: 'd',
    css: 'css',
    cs: 'csharp',
    c: 'c_cpp',
    h: 'c_cpp',
    cpp: 'c_cpp',
    hpp: 'c_cpp',
    bat: 'batchfile',
    asm: 'assembly_x86',
};